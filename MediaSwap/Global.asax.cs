﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;

namespace MediaSwap
{
	// Note: For instructions on enabling IIS6 or IIS7 classic mode, 
	// visit http://go.microsoft.com/?LinkId=9394801
	public class MvcApplication : System.Web.HttpApplication
	{
		public static ISessionFactory SessionFactory { get; protected set; }

		protected void Application_Start()
		{
			AreaRegistration.RegisterAllAreas();

			WebApiConfig.Register(GlobalConfiguration.Configuration);
			FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
			RouteConfig.RegisterRoutes(RouteTable.Routes);
			SessionFactory = CreateSessionFactory();
		}

		private static ISessionFactory CreateSessionFactory()
		{
			Action<MySQLConnectionStringBuilder> conn;
			//if (HttpContext.Current.Request.Url.Host.ToLower().Contains("localhost"))
			//    conn = (c => c.Server("localhost").Username("root").Password("eggbert").Database("twoconsent"));
			//else 
			{
				conn = (c => c.Server("xxx").Username("xxx").Password("xxx").Database("xxx"));
				conn = (c => c.Server("mysql01.anaxanet.com").Username("mediaswap_user").Password("Benjamin12").Database("mediaswap_db"));
			}
			return Fluently.Configure()
			  .ExposeConfiguration(c => c.Properties.Add("hbm2ddl.keywords", "none"))
			  .Database(MySQLConfiguration.Standard.ConnectionString(conn))
			  .Mappings(m =>
				  m.FluentMappings.AddFromAssemblyOf<MvcApplication>())
			  .ExposeConfiguration(BuildSchema)
			  .BuildSessionFactory();
		}

		private static void BuildSchema(NHibernate.Cfg.Configuration config)
		{
			new NHibernate.Tool.hbm2ddl.SchemaUpdate(config).Execute(false, true);

		}




		internal static string GetRandomStringUsingGuid()
		{
			return Convert.ToBase64String(Guid.NewGuid().ToByteArray());
		}


		private static char[] Domain = "ABCDEFGHJKMNPQRSTWXYZ123456789".ToCharArray();
		private static Random r = new Random();
		public static string GetRandomString()
		{
			//return Convert.ToBase64String(Guid.NewGuid().ToByteArray());
			char[] c = new char[5];
			for (int i = 0; i < c.Length; ++i)
				c[i] = Domain[r.Next(Domain.Length)];
			return "X" + new string(c);
		}


	}
}