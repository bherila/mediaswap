﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Web;
using Amazon.PAAPI;
using FluentNHibernate.Mapping;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Linq;

namespace MediaSwap.Models
{
	public class ProductModelMap : ClassMap<ProductModel>
	{
		public ProductModelMap()
		{
			Id(a => a.Id);
			Map(a => a.Upc, "upc");
			Map(a => a.Isbn, "isbn");
			Map(a => a.Asin);
			Map(a => a.Title, "title").Length(1024);
			Map(a => a.Format, "format");
			Map(a => a.Genre, "genre");
			Map(a => a.MediaType, "media");
			Map(a => a.Platform, "plat");
		}
	}

	public class ProductModel
	{
		public virtual int Id { get; set; }

		private int _upc;
		private int _isbn;

		public ProductModel()
		{
			Upc = Isbn = -1;
			Asin = null;
		}

		public override string ToString()
		{
			var sb = new StringBuilder(255);
			sb.Append('{');
			foreach (PropertyDescriptor descriptor in TypeDescriptor.GetProperties(this))
			{
				var name = descriptor.Name;
				var value = descriptor.GetValue(this);
				sb.AppendFormat("\t{0}={1},\n", name, value);
			}
			sb.Append('}');
			return sb.ToString();
		}

		public static void Fill(ProductModel model, Item item)
		{
			model.Asin = item.ASIN;
			int.TryParse(item.ItemAttributes.UPC, out model._upc);
			if (int.TryParse(item.ItemAttributes.ISBN, out model._isbn))
				model.Asin = null;
			model.Platform = item.ItemAttributes.HardwarePlatform;
			model.Title = item.ItemAttributes.Title;
			model.Format = String.Join(",", item.ItemAttributes.Format ?? new string[] { });
			model.Genre = item.ItemAttributes.Genre;
			model.MediaType = item.ItemAttributes.MediaType;
		}

		public virtual string Platform { get; set; }
		public virtual string Title { get; set; }
		public virtual string Format { get; set; }
		public virtual string Genre { get; set; }
		public virtual string MediaType { get; set; }
		public virtual String Asin { get; set; }
		public virtual Int32 Upc
		{
			get { return _upc; }
			set { _upc = value; }
		}
		public virtual Int32 Isbn
		{
			get { return _isbn; }
			set { _isbn = value; }
		}



		public static IEnumerable<ProductModel> Find(ISession session, string query, string index)
		{
			var result = new List<ProductModel>();
			int sInt;
			var foundExact = false;
			IEnumerable<ProductModel> en = null;
			if (int.TryParse(query, out sInt))
			{
				en = from x in session.Query<ProductModel>()
				     where x.Isbn == sInt || x.Upc == sInt || x.Asin == query
				     select x;
			}
			else
			{
				en = (from x in session.Query<ProductModel>()
				     where x.Asin == query
				     select x).ToArray();
			}

			foreach (var productModel in en)
			{
				foundExact = true;
				result.Add(productModel);
				return result;
			}

			if (!foundExact)
			{
				var crits = new List<ICriterion>();
				// ReSharper disable LoopCanBeConvertedToQuery
				foreach (var word in query.Split(' '))
					crits.Add(Restrictions.On<ProductModel>(p => p.Title).IsInsensitiveLike(word + "%"));
				// ReSharper restore LoopCanBeConvertedToQuery
				var ql = session.QueryOver<ProductModel>().Or(crits.ToArray()).List();
				if (ql.Count > 0)
					return ql;
				en = ql;
			}

			if (!foundExact)
			{
				// ReSharper disable UseObjectOrCollectionInitializer
				var request = new ItemSearchRequest() {Keywords = query};
				request.ResponseGroup = new[] {"Small"};
				request.SearchIndex = index;
				request.Keywords = query;
				request.ResponseGroup = new[] {"Small", "ItemAttributes"};
				request.MerchantId = "All";
				// ReSharper restore UseObjectOrCollectionInitializer
				var itemsearch = new ItemSearch()
					                 {
						                 Request = new[] {request},
						                 AWSAccessKeyId = ConfigurationManager.AppSettings["accessKeyId"],
						                 AssociateTag = "bherilanet-20"
					                 };
				var client = new AWSECommerceServicePortTypeClient();
				var response = client.ItemSearch(itemsearch);
				foreach (var item in response.Items[0].Item)
				{

					var pm = new ProductModel();
					Fill(pm, item);
					result.Add(pm);
					session.Save(pm);
				}

			}
			return result;
		}

		public static IEnumerable<ProductModel> Find(string query, string index)
		{
			using (var session = MvcApplication.SessionFactory.OpenSession())
				return Find(session, query, index);
		}


		/*
		 *         static void Main(string[] args)
		{
			// Instantiate Amazon ProductAdvertisingAPI client
			AWSECommerceServicePortTypeClient amazonClient = new AWSECommerceServicePortTypeClient();

			// prepare an ItemSearch request
			ItemSearchRequest request = new ItemSearchRequest();
			request.SearchIndex = "Books";
			request.Title = "WCF";
			request.ResponseGroup = new string[] { "Small" };

			ItemSearch itemSearch = new ItemSearch();
			itemSearch.Request = new ItemSearchRequest[] { request };
			itemSearch.AWSAccessKeyId = ConfigurationManager.AppSettings["accessKeyId"];

			// send the ItemSearch request
			ItemSearchResponse response = amazonClient.ItemSearch(itemSearch);

			// write out the results from the ItemSearch request
			foreach (var item in response.Items[0].Item)
			{
				Console.WriteLine(item.ItemAttributes.Title);
			}
			Console.WriteLine("done...enter any key to continue>");
			Console.ReadLine();

		}*/

	}
}