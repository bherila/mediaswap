﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Web;
using FluentNHibernate.Mapping;

namespace MediaSwap.Models
{
	public class UserModel
	{
		public virtual int Id { get; set; }
		public virtual string Firstname { get; set; }
		public virtual string Lastname { get; set; }
		public virtual string Email { get; set; }
		public virtual string PasswordHash { get; set; }
		public virtual string PasswordEntry1 { get; set; }
		public virtual string PasswordEntry2 { get; set; }
		public virtual string VarSalt { get; set; }
		public virtual bool DesignOffer { get; set; }
		public virtual bool BizOffer { get; set; }
		public virtual bool DevOffer { get; set; }
		public virtual bool EmailOptin { get; set; }
		public virtual string SignupIp { get; set; }
		public virtual DateTime SignupDate { get; set; }
		public virtual DateTime LastLogin { get; set; }
		public virtual string ConfirmToken { get; set; }

		public virtual void UpdatePassword()
		{
			if (PasswordEntry1 != PasswordEntry2)
				throw new ArgumentException("Password entries don't match!");
			PasswordHash = HashPassword(PasswordEntry1);
		}

		private static readonly SHA256CryptoServiceProvider sha2 = new SHA256CryptoServiceProvider();
		public virtual string HashPassword(string password)
		{
			if (String.IsNullOrEmpty(VarSalt))
				VarSalt = MvcApplication.GetRandomStringUsingGuid();

			var pBytes = new List<byte>(200);
			pBytes.AddRange(System.Text.Encoding.UTF8.GetBytes("lNs9lPJoq,G,YxrxDb*=8--x"));
			pBytes.AddRange(Convert.FromBase64String(VarSalt ?? string.Empty));
			pBytes.AddRange(System.Text.Encoding.UTF8.GetBytes(password ?? string.Empty));
			return Convert.ToBase64String(sha2.ComputeHash(pBytes.ToArray()));
		}
	}


	public class UserModelMap : ClassMap<UserModel>
	{
		public UserModelMap()
		{
			Id(a => a.Id);
			Map(a => a.Firstname, "first_name");
			Map(a => a.Lastname, "last_name");
			Map(a => a.Email, "email");
			Map(a => a.PasswordHash, "pwd");
			Map(a => a.VarSalt, "pwd_salt");
			Map(a => a.DesignOffer, "o_design");
			Map(a => a.BizOffer, "o_biz");
			Map(a => a.DevOffer, "o_dev");
			Map(a => a.EmailOptin, "o_optin");
			Map(a => a.SignupIp, "s_ip");
			Map(a => a.SignupDate, "s_date");
			Map(a => a.LastLogin, "last_logindate");
			Map(a => a.ConfirmToken, "email_confirm");
		}
	}

}