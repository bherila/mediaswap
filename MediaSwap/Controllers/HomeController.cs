﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MediaSwap.Models;
using NHibernate.Linq;

namespace MediaSwap.Views.Home
{
	public class HomeController : Controller
	{
		//
		// GET: /Home/

		private void SetupViewBag()
		{
			ViewBag.OK = true;
			ViewBag.EmailExists = false;
			ViewBag.PasswordNoMatch = false;
			using (var session = MvcApplication.SessionFactory.OpenSession())
			{
				var sq = (from x in session.Query<UserModel>()
				          select x);
				ViewBag.MembersNeeded = 750 - sq.Count();
			}
		}

		public ActionResult Index()
		{
			SetupViewBag();
			return View();
		}

		[HttpPost]
		public ActionResult Index(UserModel newUser)
		{
			SetupViewBag();

			using (var session = MvcApplication.SessionFactory.OpenSession())
			{
				var sq = from x in session.Query<UserModel>()
						 where x.Email == newUser.Email
						 select x;

				if (sq.Any()) // fail- user already exists!
				{
					ViewBag.EmailExists = true;
					ViewBag.OK = false;
				}

				if (newUser.PasswordEntry1 != newUser.PasswordEntry2)
				{
					ViewBag.PasswordNoMatch = true;
					ViewBag.OK = false;
				}

				if (ViewBag.OK)
					using (var trans = session.BeginTransaction())
					{
						newUser.UpdatePassword(); // hash it!
						newUser.SignupIp = Request.UserHostAddress;
						newUser.SignupDate = DateTime.UtcNow;
						newUser.ConfirmToken = MvcApplication.GetRandomString();
						session.SaveOrUpdate(newUser);
						trans.Commit();
						if (!trans.WasCommitted)
						{
							ViewBag.OK = false;
						}
					}

				if (ViewBag.OK)
				{
					return View("PreviewThanks", newUser);
				}
				return View(newUser);
			}
		}

	}
}
